from django import forms
from .models import JadwalPribadi

class Message_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    kegiatan = forms.CharField(label = "Nama kegiatan", max_length = 50, required = True, widget = forms.TextInput())
    tanggal = forms.DateField(label = "Tanggal", required = True, widget = forms.DateInput(attrs = {"type" : "date"}))
    waktu = forms.TimeField(label = "Waktu", required = True, widget = forms.TimeInput(attrs = {"type" : "time"}))
    tempat = forms.CharField(label = "Tempat", max_length = 50, required = True, widget = forms.TextInput())
    kategori = forms.CharField(label = "Kategori", max_length = 50, required = True, widget = forms.TextInput())