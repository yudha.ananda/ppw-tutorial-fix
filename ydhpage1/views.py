from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import Message_Form
from .models import JadwalPribadi

# Create your views here.
response = {'author' : 'Yudha Ananda'}
def mainmenu(request):
    return render(request, "mainmenu.html")

def datasaya(request):
    return render(request, "datasaya.html")

def proyeksaya(request):
    return render(request, "proyeksaya.html")

def qotd(request):
    return render(request, "qotd.html")

def form(request):
    return render(request, "form.html")

def formjadwal(request):
	jadwal = JadwalPribadi.objects.all()
	form = Message_Form(request.POST)
	
	if request.method == "POST":
		if "hapus" in request.POST:
			jadwal.delete()
			context = {'form': form, "jadwal": jadwal}
			return render(request, 'formjadwal.html', context)

		response['kegiatan'] = request.POST['kegiatan']
		response['tanggal'] = request.POST['tanggal']
		response['waktu'] = request.POST['waktu']
		response['tempat'] = request.POST['tempat']
		response['kategori'] = request.POST['kategori']
		resp1 = JadwalPribadi(kegiatan = response['kegiatan'], tanggal = response["tanggal"], waktu = response["waktu"], tempat = response["tempat"], kategori = response["kategori"])
		resp1.save()
		form = Message_Form()
		context = {'form': form, "jadwal": jadwal}
		return render(request, 'formjadwal.html', context)
    	
			
	else:
		form = Message_Form()
		context = {'form': form, 'jadwal': jadwal}
		return render(request, 'formjadwal.html', context)