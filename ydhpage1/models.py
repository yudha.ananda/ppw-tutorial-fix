from django.db import models
from django.utils import timezone
from datetime import date, time

class JadwalPribadi(models.Model):
    kegiatan = models.CharField(max_length = 50)
    tanggal = models.DateField(default= timezone.now)
    waktu = models.TimeField(default= timezone.now)
    tempat = models.CharField(max_length = 50)
    kategori = models.CharField(max_length = 50)