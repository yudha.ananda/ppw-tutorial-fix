from django.urls import path
from .views import mainmenu, datasaya, proyeksaya, qotd, form, formjadwal
from django.conf import settings

urlpatterns = [
    path('mainmenu', mainmenu),
    path('datasaya', datasaya),
    path('proyeksaya', proyeksaya),
    path('qotd', qotd),
    path('form', form),
    path('formjadwal', formjadwal),
	path('', mainmenu)
]

